# mars-rover-challenge


## Assumptions

- No Minus Coordinates
- No Obticals on the flow


## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com)
- [Maven 3](https://maven.apache.org)
- [Eclipse - Optional](https://www.eclipse.org)
- [Lombok ](https://projectlombok.org/) Required when you check with the IDE

## Build It Locally

```shell
mvn clean install
```

## Run It Locally

```shell
java -Dserver.port=8080 -jar mars-rover-challenge-0.0.1-SNAPSHOT.jar
```

NOTE : Make sure 8080 port is avilable to run or change the port and run


## Properties

```shell
server.servlet.context-path=/mars-rover-challenge

rotation.degree=90.0  Rotation angle

move.point=1  Number of points to move when M command execute 

```



## Postman

- [Postman Collection](https://www.getpostman.com/collections/02769c4beb8ed0728c34)



## Swagger

- [Swagger URL](http://localhost:8080/mars-rover-challenge/swagger-ui/index.html?configUrl=/mars-rover-challenge/v3/api-docs/swagger-config#/rover-rest-controller/submitRoverCommandsToGetFinalLocationDetails)



## Sample Request Json


```shell
{
    "limitPositionXY":{
        "pointX":5,
        "pointY":5
    },
    "roverDetailsDTOList":[
        {

            "currentPosotionXY":{
                "pointX":1,
                "pointY":2,
                "headDirection":"N"
            },
            "commandList":[
                "L","M", "L","M", "L","M", "L","M","M"
            ]

        },
        {
            "currentPosotionXY":{
                "pointX":3,
                "pointY":3,
                "headDirection":"E"
            },
            "commandList":[
                "M","M", "R","M", "M","R", "M","R","R","M"
            ]
        }

    ]
}

```

## Sample Response Json

```shell
{
    "result": [
        {
            "roverId": 1,
            "currentPosotionXY": {
                "pointX": 1,
                "pointY": 3,
                "headDirection": "N"
            }
        },
        {
            "roverId": 2,
            "currentPosotionXY": {
                "pointX": 5,
                "pointY": 1,
                "headDirection": "E"
            }
        }
    ],
    "status": "200 OK",
    "description": "OK"
}
```
