/**
 * 
 */
package com.techinical.test.marsroverchallenge.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techinical.test.marsroverchallenge.dto.ParameterDTO;
import com.techinical.test.marsroverchallenge.dto.ResponseDTO;
import com.techinical.test.marsroverchallenge.dto.RoverDetailsResponseDTO;
import com.techinical.test.marsroverchallenge.service.CoordinateService;

/**
 * @author Nuwan
 * 
 * API End Points For Coordinates
 *
 */

@RestController
@RequestMapping("/secure/api/command")
public class RoverRestController {
	
	
	@Autowired
	public CoordinateService coordinateService;
	
	  /**
	   * Submit rover commands to Get Final location details
	   * @return
	   */
	  @PostMapping(value={"/rover"})
	  public ResponseEntity<?> submitRoverCommandsToGetFinalLocationDetails(@Valid  @RequestBody ParameterDTO parameterDTO) {
		  
		  ResponseDTO<List<RoverDetailsResponseDTO>> resultDTO = new ResponseDTO<List<RoverDetailsResponseDTO>>();
		  
		  
		  List<RoverDetailsResponseDTO> roverDetailsResponseDTOList = coordinateService.findFinalCoordinate(parameterDTO);
		  resultDTO.setResult(roverDetailsResponseDTOList);
		  resultDTO.setStatus(HttpStatus.OK.toString());
		  resultDTO.setDescription(HttpStatus.OK.getReasonPhrase());
		  
	       return ResponseEntity.ok(resultDTO);
	   }

}
