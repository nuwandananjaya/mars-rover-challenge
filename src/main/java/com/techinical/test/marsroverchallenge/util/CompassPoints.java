/**
 * 
 */
package com.techinical.test.marsroverchallenge.util;

import org.springframework.http.HttpStatus;

import com.techinical.test.marsroverchallenge.exception.NotFoundException;

import lombok.Getter;

/**
 * @author Nuwan
 * Hold The Compass points
 *
 */

@Getter
public enum CompassPoints {
	
	N(0.0), S(180.0), W(270.0), E(90.0), N360(360.0);
	
	private Double degrees;  

	CompassPoints(Double degrees) {
	    this.degrees = degrees;
	}
	
	/**
	 * Get Compass points by degrees
	 * @param degree
	 * @return
	 */
	public static CompassPoints fromDegrees(Double degree) {
		
		Boolean isFound = false;
        for (CompassPoints compassPoints : CompassPoints.values()) {
            if (compassPoints.degrees.equals(degree)) {
            	isFound = true;
                return compassPoints;
            }
        }
        
        if(!isFound) {
        	 throw new NotFoundException(HttpStatus.NOT_FOUND.name(),HttpStatus.NOT_FOUND.getReasonPhrase());
        }
        
        return null ;
    }

}
