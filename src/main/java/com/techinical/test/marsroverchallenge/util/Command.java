/**
 * 
 */
package com.techinical.test.marsroverchallenge.util;

/**
 * @author Nuwan
 * Hold The Command
 *
 */
public enum Command {
	
	L("Left"), R("Right"), M("Move");
	
	private String description;  

	public String getDescription() {
		return description;
	}

	Command(String description) {
	    this.description = description;
	}

}
