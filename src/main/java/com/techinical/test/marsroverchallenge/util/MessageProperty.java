/**
 * 
 */
package com.techinical.test.marsroverchallenge.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 *
 */

@Getter
@Setter
@Configuration
@PropertySource("classpath:errorMessage.properties")
public class MessageProperty {
	
	@Value("${error.message.wrongParameters}")
	private String wrongParameters;
	
	@Value("${error.message.coordinateXNotInRange}")
	private String coordinateXNotInRange;
	
	@Value("${error.message.coordinateYNotInRange}")
	private String coordinateYNotInRange;
	
	@Value("${error.message.wrongCommand}")
	private String wrongCommand;
	
	
	
}
