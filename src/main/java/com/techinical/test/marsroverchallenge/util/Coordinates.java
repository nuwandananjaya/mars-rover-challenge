/**
 * 
 */
package com.techinical.test.marsroverchallenge.util;

/**
 * @author Nuwan
 * Hold The Coordinates
 *
 */
public enum Coordinates {
	
	X("X"), Y("Y");
	
	private String description;  

	public String getDescription() {
		return description;
	}

	Coordinates(String description) {
	    this.description = description;
	}

}
