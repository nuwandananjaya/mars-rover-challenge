/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.CurrentPositionDTO;
import com.techinical.test.marsroverchallenge.service.CommandService;

/**
 * @author Nuwan
 * 
 * Move North Service Implementation
 *
 */

@Service
public class MoveNorthServiceImpl implements CommandService{
	
	@Value(value = "${move.point}")
    private Integer movePoint;

	
	/**
	 * Move north
	 */
	@Override
	public  CurrentPositionDTO executeCommand(CurrentPositionDTO currentPositionDTO){
		
		Integer newPossition = currentPositionDTO.getPointY() + movePoint;
		currentPositionDTO.setPointY(newPossition);
		
		return currentPositionDTO;
	}
	
	

}
