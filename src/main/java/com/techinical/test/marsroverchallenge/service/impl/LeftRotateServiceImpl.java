/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.CurrentPositionDTO;
import com.techinical.test.marsroverchallenge.service.CommandService;
import com.techinical.test.marsroverchallenge.util.CompassPoints;

/**
 * @author Nuwan
 * 
 * Rotate Left Service Implementation
 *
 */

@Service
public class LeftRotateServiceImpl implements CommandService{
	
	
	@Value(value = "${rotation.degree}")
    private Double rotation;

	
	/**
	 * Rotate Left and set head
	 */
	@Override 
	public CurrentPositionDTO executeCommand(CurrentPositionDTO currentPositionDTO){
		
		
		Double degrees = CompassPoints.valueOf(currentPositionDTO.getHeadDirection().name()).getDegrees();
		
		if(CompassPoints.N.getDegrees().equals(degrees)) {
			degrees = CompassPoints.N360.getDegrees();
		}
		currentPositionDTO.setHeadDirection(CompassPoints.fromDegrees(degrees - rotation));
		
		return currentPositionDTO;
	}
	
	

}
