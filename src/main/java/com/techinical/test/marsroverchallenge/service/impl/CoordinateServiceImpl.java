/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.FactoryParameterDTO;
import com.techinical.test.marsroverchallenge.dto.ParameterDTO;
import com.techinical.test.marsroverchallenge.dto.RoverDetailsResponseDTO;
import com.techinical.test.marsroverchallenge.service.CommandAbstractFactoryService;
import com.techinical.test.marsroverchallenge.service.CommandService;
import com.techinical.test.marsroverchallenge.service.CoordinateService;
import com.techinical.test.marsroverchallenge.validator.CoordinateValidator;

/**
 * @author Nuwan
 * 
 * Coordinate Service Implementation
 *
 */

@Service
public class CoordinateServiceImpl implements CoordinateService{
	
	@Autowired
	private CoordinateValidator coordinateValidator;
	
	@Autowired
	private CommandAbstractFactoryProviderImpl commandFactoryProvider;

	
	/**
	 * Find final Coordinates with heading
	 */
	@Override
	public List<RoverDetailsResponseDTO> findFinalCoordinate(ParameterDTO parameterDTO) {
		
		
		List<RoverDetailsResponseDTO> roverDetailsResponseDTOList = new ArrayList<RoverDetailsResponseDTO>();
		int [] increment = {1};
		parameterDTO.getRoverDetailsDTOList().stream().forEach(roverDetailsDTO -> {
			
			RoverDetailsResponseDTO roverDetailsResponseDTO = new RoverDetailsResponseDTO();
			roverDetailsDTO.getCommandList().stream().forEach( command -> {
				
				coordinateValidator.validateCoordinateXInRange(parameterDTO.getLimitPositionXY().getPointX(), roverDetailsDTO.getCurrentPosotionXY().getPointX());
				coordinateValidator.validateCoordinateYInRange(parameterDTO.getLimitPositionXY().getPointY(), roverDetailsDTO.getCurrentPosotionXY().getPointY());
				
				CommandAbstractFactoryService commandAbstractFactoryService = commandFactoryProvider.getCommandFactory(command.name());
				
				FactoryParameterDTO factoryParameterDTO = new FactoryParameterDTO();
				factoryParameterDTO.setCommand(command);
				factoryParameterDTO.setHeadDirection(roverDetailsDTO.getCurrentPosotionXY().getHeadDirection());
				
				CommandService commandService = commandAbstractFactoryService.cretate(factoryParameterDTO);
				commandService.executeCommand(roverDetailsDTO.getCurrentPosotionXY());
				
			});
			
			roverDetailsResponseDTO.setRoverId(increment[0]);
			roverDetailsResponseDTO.setCurrentPosotionXY(roverDetailsDTO.getCurrentPosotionXY());
			roverDetailsResponseDTOList.add(roverDetailsResponseDTO);
			
			increment[0] = increment[0] + 1;
			
		});
		
		
		
		
		
		
		return roverDetailsResponseDTOList;
	}

	
	
	
	

}
