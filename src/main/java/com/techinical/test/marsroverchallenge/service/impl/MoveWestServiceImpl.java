/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.CurrentPositionDTO;
import com.techinical.test.marsroverchallenge.service.CommandService;

/**
 * @author Nuwan
 * 
 * Move West Service Implementation
 *
 */

@Service
public class MoveWestServiceImpl implements CommandService{
	
	@Value(value = "${move.point}")
    private Integer movePoint;

	
	/**
	 * Move west
	 */
	@Override
	public CurrentPositionDTO executeCommand(CurrentPositionDTO currentPositionDTO)  {
		
		Integer newPossition = currentPositionDTO.getPointX() - movePoint;
		currentPositionDTO.setPointX(newPossition);
		
		return currentPositionDTO;
	}
	
	
	

}
