/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.CurrentPositionDTO;
import com.techinical.test.marsroverchallenge.service.CommandService;
import com.techinical.test.marsroverchallenge.util.CompassPoints;

/**
 * @author Nuwan
 * 
 * Rotate Right Service Implementation
 *
 */

@Service
public class RightRotateServiceImpl implements CommandService{
	
	
	@Value(value = "${rotation.degree}")
    private Double rotation;


	
	/**
	 * Rotate Right and set head
	 */
	@Override 
	public CurrentPositionDTO executeCommand(CurrentPositionDTO currentPositionDTO)  {
		
		Double degrees = CompassPoints.valueOf(currentPositionDTO.getHeadDirection().name()).getDegrees();
		
		degrees = degrees + rotation;
		if(CompassPoints.N360.getDegrees().equals(degrees)) {
			degrees = CompassPoints.N.getDegrees();
		}
		
		currentPositionDTO.setHeadDirection(CompassPoints.fromDegrees(degrees));
		
		return currentPositionDTO;
	}
	
	

}
