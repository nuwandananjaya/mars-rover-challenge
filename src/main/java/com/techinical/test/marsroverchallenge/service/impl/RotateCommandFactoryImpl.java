/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.FactoryParameterDTO;
import com.techinical.test.marsroverchallenge.exception.NotFoundException;
import com.techinical.test.marsroverchallenge.service.CommandAbstractFactoryService;
import com.techinical.test.marsroverchallenge.service.CommandService;
import com.techinical.test.marsroverchallenge.util.Command;
import com.techinical.test.marsroverchallenge.util.MessageProperty;

/**
 * @author Nuwan
 * 
 * Hold the factory of Rotate
 *
 */

@Service
public class RotateCommandFactoryImpl  implements CommandAbstractFactoryService {
	
	@Autowired
	private LeftRotateServiceImpl leftRotateServiceImpl;
	
	@Autowired
	private RightRotateServiceImpl rightRotateServiceImpl;
	
	@Autowired
	private MessageProperty messageProperty;
	
	
	
	/**
	 * Get Rotate Service
	 * @param command
	 * @return
	 */
	  public CommandService cretate(FactoryParameterDTO factoryParameterDTO){
		  
		  String command = factoryParameterDTO.getCommand().name();
	     	
	      if(command != null && command.equalsIgnoreCase(Command.L.name())){
	         return leftRotateServiceImpl;
	         
	      } else if(command != null && command.equalsIgnoreCase(Command.R.name())){
	         return rightRotateServiceImpl;
	         
	      }else {
	    	  throw new NotFoundException(HttpStatus.NOT_FOUND.name(), messageProperty.getWrongCommand());
	      }
	  }

}
