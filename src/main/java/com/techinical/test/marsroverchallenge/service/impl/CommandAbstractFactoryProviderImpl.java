/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.service.CommandAbstractFactoryService;
import com.techinical.test.marsroverchallenge.util.Command;

/**
 * @author Nuwan
 * 
 * Abstract Factory Provider
 *
 */

@Service
public class CommandAbstractFactoryProviderImpl {
	
	@Autowired
	private MoveCommandFactoryImpl moveCommandFactoryImpl;
	
	@Autowired
	private RotateCommandFactoryImpl rotateCommandFactoryImpl;
	
	
	/**
	 * Provide the relevant factory
	 * @param command
	 * @return
	 */
	public CommandAbstractFactoryService getCommandFactory(String command){
        
		if(Command.M.name().equals(command)) {
			
			return moveCommandFactoryImpl;
			
		}else {
			
			return rotateCommandFactoryImpl;
		}
        
    }

}
