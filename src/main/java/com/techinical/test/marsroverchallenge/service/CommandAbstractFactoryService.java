package com.techinical.test.marsroverchallenge.service;

import com.techinical.test.marsroverchallenge.dto.FactoryParameterDTO;

/**
 * 
 * @author Nuwan
 * 
 * Command Abstract Factory Service
 *
 */
public interface CommandAbstractFactoryService {
	
	
	/**
	 * Base Abstract Factory create
	 * @param factoryParameterDTO
	 * @return
	 */
	 public CommandService cretate(FactoryParameterDTO factoryParameterDTO) ;

}
