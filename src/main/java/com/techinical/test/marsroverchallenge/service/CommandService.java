/**
 * 
 */
package com.techinical.test.marsroverchallenge.service;

import com.techinical.test.marsroverchallenge.dto.CurrentPositionDTO;

/**
 * 
 * Base Command Service
 * @author Nuwan
 *
 */
public interface CommandService {
	
	/**
	 * Base command service  
	 * @param currentPositionDTO
	 */
	public CurrentPositionDTO executeCommand(CurrentPositionDTO currentPositionDTO);
	

}
