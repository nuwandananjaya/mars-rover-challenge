/**
 * 
 */
package com.techinical.test.marsroverchallenge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.techinical.test.marsroverchallenge.dto.FactoryParameterDTO;
import com.techinical.test.marsroverchallenge.exception.NotFoundException;
import com.techinical.test.marsroverchallenge.service.CommandAbstractFactoryService;
import com.techinical.test.marsroverchallenge.service.CommandService;
import com.techinical.test.marsroverchallenge.util.CompassPoints;
import com.techinical.test.marsroverchallenge.util.MessageProperty;

/**
 * @author Nuwan
 * 
 * Hold the factory of Move
 *
 */

@Service
public class MoveCommandFactoryImpl implements CommandAbstractFactoryService{
	
	@Autowired
	private MoveNorthServiceImpl moveNorthServiceImpl;
	
	@Autowired
	private MoveSouthServiceImpl moveSouthServiceImpl;
	
	@Autowired
	private MoveEastServiceImpl moveEastServiceImpl;
	
	@Autowired
	private MoveWestServiceImpl moveWestServiceImpl;
	
	@Autowired
	private MessageProperty messageProperty;
	
	
	
	
	/**
	 * Get Move Service
	 * @param command
	 * @return
	 */
	  public CommandService cretate(FactoryParameterDTO factoryParameterDTOs){
	      
	      String headerDirection = factoryParameterDTOs.getHeadDirection().name();
	      
	      if(headerDirection != null && headerDirection.equalsIgnoreCase(CompassPoints.N.name())){
	    	  return moveNorthServiceImpl;
	         
	      } else if(headerDirection != null && headerDirection.equalsIgnoreCase(CompassPoints.S.name())){
	    	  return moveSouthServiceImpl;
	         
	      }else if(headerDirection != null && headerDirection.equalsIgnoreCase(CompassPoints.E.name())){
	    	  return moveEastServiceImpl;
		         
		  }else if(headerDirection != null && headerDirection.equalsIgnoreCase(CompassPoints.W.name())){
			  return moveWestServiceImpl;
		         
		  }else {
			  throw new NotFoundException(HttpStatus.NOT_FOUND.name(), messageProperty.getWrongCommand());
		  }
	      
	  }


}
