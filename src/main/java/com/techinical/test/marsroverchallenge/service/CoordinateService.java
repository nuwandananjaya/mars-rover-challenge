/**
 * 
 */
package com.techinical.test.marsroverchallenge.service;

import java.util.List;

import com.techinical.test.marsroverchallenge.dto.ParameterDTO;
import com.techinical.test.marsroverchallenge.dto.RoverDetailsResponseDTO;

/**
 * 
 * Coordinate Service Base
 * @author Nuwan
 *
 */
public interface CoordinateService {
	
	/**
	 * Base find final coordinates and headings
	 * @param parameterDTO
	 */
	public List<RoverDetailsResponseDTO> findFinalCoordinate(ParameterDTO parameterDTO);
	

}
