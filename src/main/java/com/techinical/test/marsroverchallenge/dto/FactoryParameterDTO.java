/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;

import com.techinical.test.marsroverchallenge.util.Command;
import com.techinical.test.marsroverchallenge.util.CompassPoints;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The position 
 *
 */

@Getter
@Setter
public class FactoryParameterDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6892661215911863303L;
	
	private CompassPoints headDirection;
	private Command command;

}
