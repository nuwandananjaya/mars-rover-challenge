/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import javax.validation.constraints.NotNull;

import com.techinical.test.marsroverchallenge.util.CompassPoints;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The position 
 *
 */

@Getter
@Setter
public class CurrentPositionDTO extends PositionDTO{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8568520443135915026L;
	
	@NotNull(message = "{error.message.not.null}")
	private CompassPoints headDirection;

}
