/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The position 
 *
 */

@Getter
@Setter
public class ParameterDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2548085746537188915L;
	
	@NotNull(message = "{error.message.not.null}")
	private @Valid PositionDTO limitPositionXY;
	
	@NotNull(message = "{error.message.not.null}")
	@NotEmpty(message = "{error.message.not.empty}")
	private List<RoverDetailsDTO> roverDetailsDTOList;

}
