/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The position 
 *
 */

@Getter
@Setter
public class PositionDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2548085746537188915L;
	
	@NotNull(message = "{error.message.not.null}")
	@Min(value = 0 , message = "{error.message.min.value}")
	private Integer pointX;
	
	@NotNull(message = "{error.message.not.null}")
	@Min(value = 0 , message = "{error.message.min.value}")
	private Integer pointY;

}
