/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The Rover details Response
 *
 */

@Getter
@Setter
public class RoverDetailsResponseDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7137768653232823557L;
	

	@NotNull(message = "{error.message.not.null}")
	private Integer roverId;

	@NotNull(message = "{error.message.not.null}")
	private @Valid CurrentPositionDTO currentPosotionXY;

}
