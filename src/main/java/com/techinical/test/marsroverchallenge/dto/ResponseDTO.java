/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;


/**
 * Class is implement Generic response for APIs
 * @author Nuwan
 *
 * @param <T>
 */
public class ResponseDTO <T> implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5022613339510101957L;
	
	private T result;
	private String status;
	private String description;
	
	
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
