/**
 * 
 */
package com.techinical.test.marsroverchallenge.dto;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.techinical.test.marsroverchallenge.util.Command;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Nuwan
 * 
 * Hold The Rover details
 *
 */

@Getter
@Setter
public class RoverDetailsDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1552618411989590236L;

	@NotNull(message = "{error.message.not.null}")
	private @Valid CurrentPositionDTO currentPosotionXY;
	
	@NotNull(message = "{error.message.not.null}")
	@NotEmpty(message = "{error.message.not.empty}")
	private List<@Valid Command> commandList;

}
