/**
 * 
 */
package com.techinical.test.marsroverchallenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Nuwan
 * 
 * Coordinate Not Found Exception Implementation
 *
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8243538998468577824L;

	public NotFoundException(String errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public NotFoundException(String errorCode, String errorDescription) {
		super(errorCode, errorDescription);
	}

	public NotFoundException(String errorCode, String errorDescription, Throwable cause) {
		super(errorCode, errorDescription, cause);
	}

}
