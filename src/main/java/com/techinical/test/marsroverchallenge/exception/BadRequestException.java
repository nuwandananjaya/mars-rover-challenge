/**
 * 
 */
package com.techinical.test.marsroverchallenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Nuwan
 * 
 * Bad Request Exception Implementation
 *
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends BaseException{


	/**
	 * 
	 */
	private static final long serialVersionUID = -2321044316170044670L;
	

	public BadRequestException(String errorCode, Throwable cause) {
		super(errorCode, cause);
	}

	public BadRequestException(String errorCode, String errorDescription) {
		super(errorCode, errorDescription);
	}

	public BadRequestException(String errorCode, String errorDescription, Throwable cause) {
		super(errorCode, errorDescription, cause);
	}

}
