/**
 * 
 */
package com.techinical.test.marsroverchallenge.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.techinical.test.marsroverchallenge.exception.NotFoundException;
import com.techinical.test.marsroverchallenge.util.MessageProperty;

/**
 * @author Nuwan
 * 
 * Coordinate validator
 *
 */

@Component
public class CoordinateValidator {
	
	@Autowired
	private MessageProperty mesgMessageProperty;
	
	
	/**
	 * Validate rover coordinates X in range
	 * @return
	 */
	public void validateCoordinateXInRange(Integer gridPointX, Integer currentPositionX) {
		
		if(gridPointX < currentPositionX || currentPositionX < 0) {
			throw new NotFoundException(HttpStatus.NOT_FOUND.name(), mesgMessageProperty.getCoordinateXNotInRange());
		}
		
	}
	
	/**
	 * Validate rover coordinates Y in range
	 * @return
	 */
	public void validateCoordinateYInRange(Integer gridPointY, Integer currentPositionY) {
		
		if(gridPointY < currentPositionY || currentPositionY < 0) {
			throw new NotFoundException(HttpStatus.NOT_FOUND.name(), mesgMessageProperty.getCoordinateYNotInRange());
		}
	}

}
